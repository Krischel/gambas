#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: database 3.13.90\n"
"PO-Revision-Date: 2019-05-18 17:09 UTC\n"
"Last-Translator: benoit <benoit@benoit-kubuntu>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Database application"
msgstr "Application de base de données"

#: .project:2
msgid "A graphical application accessing a database."
msgstr "Une application graphique accédant à une base de donneés"


#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2014-09-23 01:18+0100\n"
"Last-Translator: Willy Raets <willy@develop.earthshipeurope.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Object manipulation example"
msgstr "Object manipulatie voorbeeld"

#: FStart.class:29
msgid "You need to create the Thing first!"
msgstr "Je dient het Ding! eerst te creëren!"

#: FStart.form:12
msgid "Object "
msgstr "Object "

#: FStart.form:18
msgid "create the Thing !"
msgstr "creëer het Ding!"

#: FStart.form:23
msgid "check the Thing !"
msgstr "controleer het Ding!"

#: FStart.form:33
msgid "destroy the Thing !"
msgstr "Vernietig het Ding!"

#: FStart.form:38
msgid " by juergen@zdero.com"
msgstr " door juergen@zdero.com"

